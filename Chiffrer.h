/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Entête de Chiffer                                               *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Savalle-Alexandre                                            *
*                                                                             *
*  Nom-prénom2 : Bessier-Jason                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : Chiffrer.h                                                *
*                                                                             *
******************************************************************************/

char chiffrerCaractereCesar(int cle, char c);
void chiffrerMessageCesar(int cle, char * message);
char chiffrerCaractereVigenere(char cle, char c);
void chiffrerMessageVigenere(char * cle, char * message);