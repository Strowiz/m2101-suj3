/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Corps de Déchiffer                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Savalle-Alexandre                                            *
*                                                                             *
*  Nom-prénom2 : Bessier-Jason                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : Déchiffrer.c                                              *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dechiffrer.h"
#include "chiffrer.h"

void dechiffrerMessageCesar(int cle, char * message)
{

	chiffrerMessageCesar(-cle, message);

}

void dechiffrerMessageVigenere(char * cle, char * message)
{

	int i = 0;
	int size = strlen(cle);
	char newCle[size + 1];

	while (cle[i] != '\0') {
		if (cle[i] >= 'a' && cle[i] <= 'z') {
			newCle[i] = (26 - (cle[i] - 'a')) + 'a';
		} else {
			newCle[i] = (26 - (cle[i] - 'A')) + 'A';
		}
		i++;
	}

	newCle[i] = '\0';
	chiffrerMessageVigenere(newCle, message);
}