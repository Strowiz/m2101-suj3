/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Entête de Afficher                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Savalle-Alexandre                                            *
*                                                                             *
*  Nom-prénom2 : Bessier-Jason                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : Afficher.h                                                *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

void choixTexte();
void choixCleCesar();
void choixCleVigenere();
void choixAlgorithme();
void choixDechiffrer();
void afficherTexte(char * message);
void afficherTexteChiffrer(char * message);
void afficherTexteDechiffrer(char * message);
void messageErreur();