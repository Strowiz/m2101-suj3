/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Main                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Savalle-Alexandre                                            *
*                                                                             *
*  Nom-prénom2 : Bessier-Jason                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : main.c                                                    *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "verif_aphanumerique.h"
#include "chiffrer.h"
#include "dechiffrer.h"

void main() {
	char message[100];
	char cle[100];
	int cle2;
	int choixAlgo;
	int choixChiffre;
	int choixDechiffre;
	int i = 0;

	choixTexte();
	fgets(message, 100, stdin);
	while (message[i] != '\n') {
		i++;
	}
	message[i] = '\0';
	if (verification(message) == false) {
		exit(messageErreur());
	}
	choixAlgorithme();
	scanf("%d", &choixAlgo);
	if (choixAlgo == 1) {
		choixCleCesar();
		scanf("%d", &cle2);
	} else {
		choixCleVigenere();
		scanf("%s", &message);
	}

	if (choixAlgo == 1) {
		chiffrerMessageCesar(cle2, message);
		afficherTexteChiffrer(message);
	} else {
		chiffrerMessageVigenere(cle,message);
		afficherTexteChiffrer(message);
	}

	choixDechiffrer();
	scanf("%d", &choixDechiffre);

	if (choixDechiffre == 1 && choixAlgo == 1) {
		dechiffrerMessageCesar(cle2, message);
		afficherTexteDechiffrer(message);
	} else if (choixDechiffre == 1 && choixAlgo == 2) {
		dechiffrerMessageVigenere(cle, message);
		afficherTexteDechiffrer(message);
	}
}
