/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Corps de Afficher                                               *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Savalle-Alexandre                                            *
*                                                                             *
*  Nom-prénom2 : Bessier-Jason                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : Afficher.c                                                *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "affichage.h"

void choixTexte()
{
	printf("Ecrivez le message à crypter\n");
}

void choixCleCesar()
{
	printf("Tapez le nombre de décalage du message\n");
}

void choixCleVigenere()
{
	printf("Ecrivez la clé de cryptage\n");
}

void choixAlgorithme()
{
	printf("Tapez 1 pour l'algorithme de César ou 2 pour l'algorithme de Vigenère\n");
}

void choixDechiffrer()
{
	printf("Tapez 1 pour déchiffrer ou 2 pour terminer\n");
}

void afficherTexte(char * message)
{
	printf("%s\n", message);
}

void afficherTexteChiffrer(char * message)
{
	printf("Voici le message crypté : %s\n", message);
}

void afficherTexteDechiffrer(char * message)
{
	printf("Voici le message décrypté : %s\n", message);
}
void messageErreur()
{
	printf("Erreur : Le texte contient des caractères spéciaux\n");
}