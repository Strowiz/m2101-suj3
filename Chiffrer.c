/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Corps de Chiffer                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Savalle-Alexandre                                            *
*                                                                             *
*  Nom-prénom2 : Bessier-Jason                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : Chiffrer.c                                                *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "chiffrer.h"

char chiffrerCaractereCesar(int cle, char c)
{


	if (c >= 'a' && c <= 'z') {
		return (c - 'a' + cle) % 26 + 97;
	} else if (c >= 'A' && c <= 'Z') {
		return (c - 'A' + cle) % 26 + 65;
	} else {
		return c;
	}
}

void chiffrerMessageCesar(int cle, char * message)
{

	int i = 0;

	while (message[i] != '\0') {
		message[i] = chiffrerCaractereCesar(cle, message[i]);
		i++;
	}

}

char chiffrerCaractereVigenere(char cle, char c)
{

	if (c >= 'a' && c <= 'z') {
		if (cle >= 'a' && cle <= 'z') {
			return (c + cle - 'a' - 'a') % 26 + 97;
		} else if (cle >= 'A' && cle <= 'Z') {
			return (c + cle - 'a' - 'A') % 26 + 97;
		}
	} else if ( c >= 'A' && c <= 'Z') {
		if (cle >= 'a' && cle <= 'z') {
			return (c + cle - 'A' - 'a') % 26 + 65;
		} else if (cle >= 'A' && cle <= 'Z') {
			return (c + cle - 'A' - 'A') % 26 + 65;
		}
	}
}

void chiffrerMessageVigenere(char * cle, char * message)
{

	int i = 0;
	int size = strlen(cle) + 1;

	while (message[i] != '\0') {
		message[i] = chiffrerCaractereVigenere(cle[i%size], message[i]);
		i++;
	}
}