# M2101-SUJ3

Notre groupe constitué d'Alexandre Savalle et Jason Bessier vous présente le projet sur lequel nous avons travaillé.
Le sujet concerne le chiffrement de messages.

Le programme fonctionne de manière suivante :
Nous entrons tout d'abord un message que nous voulons crypter dans la console, celui-ci sera chiffré suivant la méthode demandé par l'utilisateur, à savoir le chiffrement de Vigenere ou bien le chiffrement de César.
Après introduction de la phrase à crypter dans la console, une vérification des caractères sera effectué afin de vérifier qu'ils soient conformes à la méthode de cryptage puis seront ainsi traité pour obtenir le message crypté en sortie.
Le message complétement crypté sera alors révélé sur l'écran de l'utilisateur.

Nos fonctions sont les suivantes :

char chiffrerCaractereCesar(int cle, char c);
-- entrée : int cle, char c
-- sortie : char
-- Cette fonction nous permet de chiffrer un caractère en suivant la méthode du chiffrement de Cesar et le renvoie


void chiffrerMessageCesar(int cle, char * message);
-- entrée : int cle, char * message
-- sortie :
-- Cette fonction fait appel à celle décrite précédemment afin de chiffrer toute une ligne de caractères grâce au chiffrement de Cesar


char chiffrerCaractereVigenere(char cle, char c);
-- entrée : char cle, char c
-- sortie : char
-- Cette fonction chiffre un caractère en effectuant la méthode du chiffrement de Vigenere puis le renvoie


void chiffrerMessageVigenere(char * cle, char * message);
-- entrée : char * cle, char * message
-- sortie :
-- Cette fonction fait appel à la fonction ci-dessus afin de chiffrer toute une ligne de caractères grâce au chiffrement de Vigenere


bool verification(char * message);
-- entrée : char * message
-- sortie : boolean
-- Cette fonction vérifie qu'il n'y ai pas de caractères spéciaux dans le message saisi par l'utilisateur


void choixTexte ();
-- entrée :
-- sortie :
-- Cette fonction demande à l'utilisateur de taper le mot ou la phrase qu'il veut crypter


void choixCleCesar();
-- entrée :
-- sortie :
-- Demande à l'utilisateur la clé pour un chiffrement de César


void choixCleVigenere();
-- entrée :
-- sortie :
-- Demande à l'utilisateur la clé pour un chiffrement de Vigenere


void choixAlgorithme();
-- entrée :
-- sortie :
-- Demande à l'utilisateur s'il préfère utiliser le chiffrement de César ou celui de Vigenere


void choixDechiffrer();
-- entrée :
-- sortie :
-- Demande à l'utilisateur s'il veut déchiffrer le message sinon laisse le message tel quel


void afficherTexte(char * message);
-- entrée : char * message
-- sortie :
-- Affiche le message que l'utilisateur a entré au début


void afficherTexteChiffrer(char * message);
-- entrée : char * message
-- sortie :
-- Affiche la version chiffré du message que l'utlisateur a entré au début


void afficherTexteDechiffrer(char * message);
-- entrée : char * message
-- sortie :
-- Affiche la version déchiffré du message chiffré précédemment


void messageErreur();
-- entrée :
-- sortie :
-- Affiche une ligne démontrant qu'une erreur s'est produite lors de la saisie de l'utilisateur


void dechiffrerMessageVigenere(char * cle, char * message);
-- entrée : char * cle, char * message
-- sortie :
-- Cette fonction dechiffre un message encrypté à la méthode de Vigenere


void dechiffrerMessageCesar(int cle, char * message);
-- entrée : int cle, char * message
-- sortie :
-- Cette fonction dechiffre un message encrypté à la méthode de Cesar