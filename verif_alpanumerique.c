/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Corps de Vérification alphanumérique                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Savalle-Alexandre                                            *
*                                                                             *
*  Nom-prénom2 : Bessier-Jason                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : verif_alpanumerique.c                                     *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "verif_aphanumerique.h"

bool verification (char * message)
{

	int i = 0;

	while (message[i] != 0) {
		if (!(message[i] >= 'A' && message[i] <= 'Z')) {
			if (!(message[i] >= 'a' && message[i] <= 'z')) {
				return false;
			}
		}
		if (!(message[i] >= 'a' && message[i] <= 'z')) {
			if (!(message[i] >= 'A' && message[i] <= 'Z')) {
				return false;
			}
		}
		i++;
	}
	return true;
}